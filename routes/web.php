<?php

use App\Http\Controllers\CategoriaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//primera ruta en Laravel 
/*Route::get( "prueba", function () {
    echo "Hola mundo";
}); */

//Ruta de controladorDB
Route::get('categorias', "CategoriaController@index"); //Rutas de get son las que podemos invocar de una URL
//Ruta de mostrar el formulario para crear categoria
Route::get('categorias/create', "CategoriaController@create");
//Ruta para guardar la nueva categoria en BD
Route::post('categorias/store' , "CategoriaController@store");//Post debido a que llegan de un formulario, No van a ser expuestos en la URL, van a viajar escondidos
//Edit se pone get porque vamos a acceder a dicha ruta atraves de un hipervinculo 
Route::get('categorias/edit/{category_id}' , "CategoriaController@edit");//nuestras rutas ahora van a esperar datos por poner los parametros "{category_id}
Route::post('categorias/update/{category_id}' , "CategoriaController@update");//Ruta para Actualizar
 