<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator; //Illuminate es de la librería de Laravel y ésta es la dependencia para el validador[]

class CategoriaController extends Controller
{   //METODOS!
    //Accion:Es un metodo del controlador, contienen el código a ejecutar
    //Nombre: Puede ser cualquiera
    //recomendado el nombre en minuscula 
    public function index(){

        //Seleccionar categorias existentes
        $categorias = Categoria::paginate(5);    //"paginate" para mostrar pedazos de datos, "all" lo muestra todo de chorro

        //Enviar la colección de categorias a una vista y las vamos a mostrar allí
        return view("categorias.index")->with("categorias" , $categorias);       
    }

    //Mostrar el formulario de crear la caterogia
    public function create(){
        //echo "Formulario de añadir Categoría";
        return view('categorias.new');
    }

    //Llegar los datos desde el formulario y guardar la categoria en BD
    public function store(Request $r){

        //ValidacionBD
        //1. Establecer las reglas de validación para cada campo
        $reglas = [
            "categoria" => ["required" , "alpha"]
        ];

        //Mensajes personalizados de las señales de las reglas
        $mensajes = [
            "required" => "Campo requerido", 
            "alpha" => "Solo letras"
        ];

        //2. Crear el objeto validador
        $validador = Validator::make($r->all() , $reglas, $mensajes);  //Los datos que voy a validar y las reglas

        //3. Validar : metodo fails
        //Fails es un metodo que te devuelve un verdadero o un falso 
        //              Retorna true(v) si la validación falla
        //              Retorna False en caso de que los datos sean correctos
        if ($validador->fails()){
            //Codigo para cuando falla la validación
            return redirect("categorias/create")->withErrors($validador);
        } else{
            //Codigo para la validación es correcta

        }


        //$_POST= arreglo por defecto de PHP
        //Almacena la información que viene desde formularios
            //var_dump($_POST);
        //Crear una nueva categoría
        $categoria = new Categoria();
        //Asignar el nombre a esa nueva Categoria
            // $categoria->name = $_POST["categoria"];//Categoria es igual al nombre del campo en el New
        $categoria->name = $r->input("categoria");  //Trae los datos desde el campo categoria
        //Guardar la nueva categoria
        $categoria->save();
        //Letrero de Exito
            //echo "Categoria Guardada"; 

        //Después de guardada la categoria.. Redirección con datos sesion:
        return redirect('categorias/create')->with("mensaje" , "Categoria Guardada");
        
    }


    public function edit($category_id){
        //Se imprime para mirar si funciona 
        //echo $category_id;
        //Seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);//Find me permite encontrar una categoria por su ID

        //Mostrar la vista de actulizar categoria, llevando dentro la categoria
        return view("categorias.edit")->with("categoria", $categoria);   //Wtih mete datos a una vista o a una ruta en particular

    }

    //Metodos para la ruta de Update que se encuentra en el Web.php
    public function update($category_id){
        //echo $category_id;
        $categoria = Categoria::find($category_id);//Find me permite encontrar una categoria por su ID
        //Editar sus Atributos
        $categoria->name = $_POST["categoria"];
        //Guardo cambios
        $categoria->save();
        //Retornar al formulario de edit
        return redirect("categorias/edit/$category_id")->with("mensaje" , "Categoria Editada Correctamente");
    }
}
