<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha256-0XAFLBbK7DgQ8t7mRWU5BF2OMm9tjtfH945Z7TTeNIo=" crossorigin="anonymous" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Añadir Categoría</title>
</head>
<body>
        <!-- Si la variable de sesión "mensaje existe, la muestre" -->
        @if(session("mensaje"))
            <p class="alert-success"> {{    session("mensaje")    }} </p>
        @endif


<form method="POST" action="{{ url('categorias/store') }}" class="form-horizontal"> <!-- Metodo por el cual van los datos por el http y el action para saber a donde van los datos -->
        @csrf <!-- Si alguien intenta acceder a Store sin usar el formulario le cancela la ejecución -->

        <fieldset>
        
        <!-- Form Name -->
        <legend>Nueva Categoría</legend>

                
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="textinput">Categoría</label>  
          <div class="col-md-4">
          <input id="textinput" name="categoria"  type="text" placeholder="Inserte Nombre Categoría:" class="form-control input-md"> <!-- Con el Name reconoce este campo de texto ante PHP -->
            <strong class="text-danger"> {{  $errors->first("categoria")   }} </strong>
          </div>
        </div>
        
        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label" for=""></label>
          <div class="col-md-4">
            <button id="" name="" class="btn btn-primary">Enviar</button>
          </div>
        </div>
        
        </fieldset>
        </form>
</body>
</html>